﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01
{
    class Program
    {
        static void newMethod ()
        {
            Console.WriteLine("This a method.");
            Console.WriteLine("This second line is now printed to the screen.");
            Console.ReadLine();
        }

        static void Main (string[] args)
        {
            newMethod();
            newMethod();
            newMethod();
            newMethod();
        }

    }
}
